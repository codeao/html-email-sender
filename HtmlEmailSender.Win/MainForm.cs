﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using HtmlEmailSender.Core;

namespace HtmlEmailSender.Win {
    public partial class MainForm : Form {
        private string m_FilePath;
        private string m_UserName;
        private string m_Password;
        private string m_recipients;
        private string m_subject;

        /// <summary>
        /// Gets the client credentials.
        /// </summary>
        public NetworkCredential ClientCredentials {
            get {
                return new NetworkCredential(m_UserName, m_Password);
            }
        }

        public string FilePath {
            get { return m_FilePath; }
            set {
                m_FilePath = value;
                lblFilePath.Text = value;
            }
        }

        public string Url {
            get { return txtWebDownloadUrl.Text; }
            set {
                txtWebDownloadUrl.Text = value;
            }
        }

        public string Recipients {
            get { return txtRecipients.Text; }
            set {
                txtRecipients.Text = value;
            }
        }

        public string Subject {
            get { return txtSubject.Text; }
            set {
                txtSubject.Text = value;
            }
        }

        public List<string> SendToAddresses {
            get { return Recipients.Split(';').ToList(); }
        }

        public MainForm() {
            InitializeComponent();

            ToggleSource();
            m_UserName = ConfigurationManager.AppSettings.Get("username");
            m_Password = ConfigurationManager.AppSettings.Get("password");
            Recipients = ConfigurationManager.AppSettings.Get("recipients");
            FilePath = ConfigurationManager.AppSettings.Get("File.Path");

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("Subject"))) {
                Subject = string.Format("{0} {1}", ConfigurationManager.AppSettings.Get("Subject"),
                                          DateTime.Now.ToString());
            }

        }

        private void btnChooseHtml_Click(object sender, EventArgs e) {
            FileDialog.ShowDialog();
            FilePath = FileDialog.FileName;
        }

        private void btnSend_Click(object sender, EventArgs e) {
            var html = string.Empty;
            IHtmlExtractor htmlExtractor = new HtmlExtractor();

            if (rbFile.Checked) {
                if (string.IsNullOrEmpty(FilePath)) {
                    MessageBox.Show(@"You must select an HTML file to send.", @"Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return;
                }

                html = htmlExtractor.ExtractFromFile(FilePath);
            }

            if (rbWebDownload.Checked) {
                if (string.IsNullOrEmpty(Url)) {
                    MessageBox.Show(@"You provide a url of the Html you would like to download.", @"Error", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    return;
                }

                html = htmlExtractor.DownloadFromWeb(Url);
            }

            if (string.IsNullOrEmpty(m_UserName) || string.IsNullOrEmpty(m_Password)) {
                CredentialsForm cf = new CredentialsForm();
                var result = cf.ShowDialog();

                if (result == DialogResult.OK) {
                    m_UserName = cf.Username;
                    m_Password = cf.Password;

                    SendEmail(html);
                }
                else {
                    return;
                }
            }
            else {
                SendEmail(html);
            }
        }

        private void SendEmail(string html) {
            try {
                IHtmlEmailSender sender = new GmailSender(ClientCredentials);
                MailMessage message = new MailMessage()
                {
                    Subject = Subject,
                    From = new MailAddress(m_UserName),
                    Sender = new MailAddress(m_UserName),
                    Body = html,
                    IsBodyHtml = true
                };
                SendToAddresses.ForEach(sta => message.To.Add(new MailAddress(sta)));
                sender.SendMail(message);
                MessageBox.Show(@"Email sent successfully", @"Success", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rbFile_CheckedChanged(object sender, EventArgs e) {
            ToggleSource();
        }

        private void rbWEbDownload_CheckedChanged(object sender, EventArgs e) {
            ToggleSource();
        }

        private void ToggleSource() {
            btnChooseHtml.Visible = rbFile.Checked;
            txtWebDownloadUrl.Visible = rbWebDownload.Checked;
            lblWebAddress.Visible = rbWebDownload.Checked;
            lblFilePath.Visible = rbFile.Checked;
        }

        private void btnClose_Click(object sender, EventArgs e) {
            Application.Exit();
        }
    }
}
