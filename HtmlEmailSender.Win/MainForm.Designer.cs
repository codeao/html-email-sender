﻿namespace HtmlEmailSender.Win {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.txtRecipients = new System.Windows.Forms.TextBox();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnChooseHtml = new System.Windows.Forms.Button();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.FileDialog = new System.Windows.Forms.OpenFileDialog();
            this.grpHtmlSource = new System.Windows.Forms.GroupBox();
            this.lblWebAddress = new System.Windows.Forms.Label();
            this.txtWebDownloadUrl = new System.Windows.Forms.TextBox();
            this.rbWebDownload = new System.Windows.Forms.RadioButton();
            this.rbFile = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpHtmlSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(12, 15);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(43, 13);
            this.lblSubject.TabIndex = 0;
            this.lblSubject.Text = "Subject";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(12, 38);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(57, 13);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "Recipients";
            // 
            // txtRecipients
            // 
            this.txtRecipients.Location = new System.Drawing.Point(105, 38);
            this.txtRecipients.Name = "txtRecipients";
            this.txtRecipients.Size = new System.Drawing.Size(167, 20);
            this.txtRecipients.TabIndex = 3;
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(105, 12);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(167, 20);
            this.txtSubject.TabIndex = 4;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(284, 165);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 5;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnChooseHtml
            // 
            this.btnChooseHtml.Location = new System.Drawing.Point(7, 53);
            this.btnChooseHtml.Name = "btnChooseHtml";
            this.btnChooseHtml.Size = new System.Drawing.Size(75, 23);
            this.btnChooseHtml.TabIndex = 6;
            this.btnChooseHtml.Text = "Choose Html";
            this.btnChooseHtml.UseVisualStyleBackColor = true;
            this.btnChooseHtml.Click += new System.EventHandler(this.btnChooseHtml_Click);
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoSize = true;
            this.lblFilePath.Location = new System.Drawing.Point(88, 58);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(0, 13);
            this.lblFilePath.TabIndex = 7;
            this.lblFilePath.Visible = false;
            // 
            // FileDialog
            // 
            this.FileDialog.FileName = "openFileDialog1";
            // 
            // grpHtmlSource
            // 
            this.grpHtmlSource.Controls.Add(this.lblWebAddress);
            this.grpHtmlSource.Controls.Add(this.lblFilePath);
            this.grpHtmlSource.Controls.Add(this.txtWebDownloadUrl);
            this.grpHtmlSource.Controls.Add(this.rbWebDownload);
            this.grpHtmlSource.Controls.Add(this.rbFile);
            this.grpHtmlSource.Controls.Add(this.btnChooseHtml);
            this.grpHtmlSource.Location = new System.Drawing.Point(15, 66);
            this.grpHtmlSource.Name = "grpHtmlSource";
            this.grpHtmlSource.Size = new System.Drawing.Size(344, 93);
            this.grpHtmlSource.TabIndex = 8;
            this.grpHtmlSource.TabStop = false;
            this.grpHtmlSource.Text = "Html Source";
            // 
            // lblWebAddress
            // 
            this.lblWebAddress.AutoSize = true;
            this.lblWebAddress.Location = new System.Drawing.Point(13, 58);
            this.lblWebAddress.Name = "lblWebAddress";
            this.lblWebAddress.Size = new System.Drawing.Size(55, 13);
            this.lblWebAddress.TabIndex = 9;
            this.lblWebAddress.Text = "Web URL";
            // 
            // txtWebDownloadUrl
            // 
            this.txtWebDownloadUrl.Location = new System.Drawing.Point(88, 55);
            this.txtWebDownloadUrl.Name = "txtWebDownloadUrl";
            this.txtWebDownloadUrl.Size = new System.Drawing.Size(237, 20);
            this.txtWebDownloadUrl.TabIndex = 9;
            // 
            // rbWebDownload
            // 
            this.rbWebDownload.AutoSize = true;
            this.rbWebDownload.Location = new System.Drawing.Point(54, 20);
            this.rbWebDownload.Name = "rbWebDownload";
            this.rbWebDownload.Size = new System.Drawing.Size(125, 17);
            this.rbWebDownload.TabIndex = 8;
            this.rbWebDownload.TabStop = true;
            this.rbWebDownload.Text = "Download From Web";
            this.rbWebDownload.UseVisualStyleBackColor = true;
            this.rbWebDownload.CheckedChanged += new System.EventHandler(this.rbWEbDownload_CheckedChanged);
            // 
            // rbFile
            // 
            this.rbFile.AutoSize = true;
            this.rbFile.Location = new System.Drawing.Point(7, 20);
            this.rbFile.Name = "rbFile";
            this.rbFile.Size = new System.Drawing.Size(41, 17);
            this.rbFile.TabIndex = 7;
            this.rbFile.TabStop = true;
            this.rbFile.Text = "File";
            this.rbFile.UseVisualStyleBackColor = true;
            this.rbFile.CheckedChanged += new System.EventHandler(this.rbFile_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(203, 165);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 196);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpHtmlSource);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.txtRecipients);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.lblSubject);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Gmail HTML email Sender";
            this.grpHtmlSource.ResumeLayout(false);
            this.grpHtmlSource.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.TextBox txtRecipients;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnChooseHtml;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.OpenFileDialog FileDialog;
        private System.Windows.Forms.GroupBox grpHtmlSource;
        private System.Windows.Forms.Label lblWebAddress;
        private System.Windows.Forms.TextBox txtWebDownloadUrl;
        private System.Windows.Forms.RadioButton rbWebDownload;
        private System.Windows.Forms.RadioButton rbFile;
        private System.Windows.Forms.Button btnClose;
    }
}