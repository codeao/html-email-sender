﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HtmlEmailSender.Win {
    public partial class CredentialsForm : Form {
        private string m_username;
        private string m_password;

        public string Username {
            get { return m_username; }
            set { 
                m_username = value;
                txtUsername.Text = value;
            }
        }

        public string Password {
            get { return m_password; }
            set {
                txtPassword.Text = value;
                m_password = value;
            }
        }

        public CredentialsForm() {
            InitializeComponent();
            Username = ConfigurationManager.AppSettings.Get("username");
            Password = ConfigurationManager.AppSettings.Get("password");
        }

        private void btnSubmit_Click(object sender, EventArgs e) {
            Password = txtPassword.Text;
            Username = txtUsername.Text;

            if(string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(Username)) {
                MessageBox.Show(@"Username and password cannot be empty.");
                return;
            } else {
                this.DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            Close();
        }

        private void CredentialsForm_Load(object sender, EventArgs e) {

        }
    }
}
