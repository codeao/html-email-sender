using System.Net.Mail;

namespace HtmlEmailSender.Core {
    public interface IHtmlEmailSender {
        void SendMail(MailMessage message); 
    }
}