using System.Net;
using System.Net.Mail;

namespace HtmlEmailSender.Core {
    public class BaseHtmlEmailSender : IHtmlEmailSender {
        private NetworkCredential m_Credentials;
        private SmtpClient m_Client; 

        public BaseHtmlEmailSender(NetworkCredential credentials, string host, int port, bool useSSL = true) {
            m_Credentials = credentials;
            m_Client = new SmtpClient();
            m_Client.Host = host;
            m_Client.Port = port;
            m_Client.EnableSsl = useSSL;
            m_Client.Credentials = m_Credentials;
        }
        
        public void SendMail(MailMessage message) {
            m_Client.Send(message);
        }
    }
}