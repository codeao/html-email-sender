using System;
using System.IO;
using System.Net;

namespace HtmlEmailSender.Core {
    public class HtmlExtractor : IHtmlExtractor {
        public string ExtractFromFile(string filePath) {
            FileInfo fi = new FileInfo(filePath);
            string html = string.Empty;

            using (StreamReader reader = new StreamReader(fi.OpenRead())) {
                html = reader.ReadToEnd();
            }

            return html;
        }

        public string DownloadFromWeb(string address) {
            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            var html = client.DownloadString(new Uri(address));
            return html;
        }
    }
}