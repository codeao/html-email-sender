﻿using System.IO;

namespace HtmlEmailSender.Core {
    public interface IHtmlExtractor {
        string ExtractFromFile(string filePath);
        string DownloadFromWeb(string address);
    }
}