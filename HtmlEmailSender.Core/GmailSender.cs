﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace HtmlEmailSender.Core {
    public class GmailSender : BaseHtmlEmailSender {
        public GmailSender(NetworkCredential credential) : base(credential, "smtp.gmail.com", 587) { }
    }
}
